<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello', function () {
    $firstName = "Howell";
    $lastName = "Martinez";
    $fullName = $firstName . " " . $lastName; // Howell Martinez
    $age = 60;
    echo "Hello there $fullName! You are $age years old!";
});

Route::get('/dog', function () {
    $dogName = "Boochie";
    $dogAge = 7;
    echo "My dog's name is " . $dogName . " She is " . $dogAge . "years old. In human years that would be "
        . $dogAge * 7 . " years old.";
});

Route::get('/boolean', function () {
    $age = 50;
    $status = 'pwd';

    // Evaluate condition
    if ($age >= 65) {
        echo 'You are a senior citizen';
    }

    if ($status == 'pwd') {
        echo 'You are a PWD';
    }

    echo 'Have a nice day!';
});

Route::get('/input', function () {
    return view('input');
});

Route::post('/accept', function () {
    $dogName = request()->input('dog_name');
    $dogAge = request()->input('dog_age');
    $dogGender = request()->input('dog_gender');
    $pronoun = "";
    // M / F
    if ($dogGender == 'M') {
        $pronoun = "He";
    } else {
        $pronoun = "She";
    }
    $dogHumanAge = $dogAge * 7;
    return view('results', [
        'dogName' => $dogName,
        'dogAge' => $dogAge,
        'dogHumanAge' => $dogHumanAge,
        'pronoun' => $pronoun,
    ]);
    // if the human age >= 65, "he/she is a senior citizen"
    // if the human age < 65, "he/she is a young dog"
});

Route::get('/string-comparison', function () {
    $fruit1 = 'apple';
    $fruit2 = 'ApPlE';

    if (strtoupper($fruit1) == strtoupper($fruit2)) {
        echo "They are the same";
    } else {
        echo "They are different";
    }
});

Route::get('/home', function () {
    return view('home');
});


Route::post('/display', function () {
    $age = request()->input('age');
    $pwd = request()->has('pwd'); // returns either true or false
    $city = request()->input('city');

    $ageResult = "";
    $pwdResult = "";

    if ($age >= 65) {
        $ageResult = "You are a senior citizen";
    }

    if ($pwd) {
        $pwdResult = "You are a PWD";
    }

    return view('display', [
        'age_message' => $ageResult,
        'status_message' => $pwdResult,
        'gender' => request()->input('gender'),
        'city' => $city,
        'birthday' => request()->input('birthday')
    ]);
});

Route::get('/test', function () {

    $birthday = "TODAY";
    $cookie = "Monster";

    return view('test', [
        'kaarawan' => $cookie,
    ]);
});

Route::get('/start', function () {
    return view('start');
});

Route::post('/weapon', function () {
    $weapon = request()->input('weapon');
    if ($weapon == 'Axe') {
        return view('path', [
            'weapon' => $weapon,
        ]);
    } else {
        return view('companion', [
            'weapon' => $weapon
        ]);
    }
});

Route::post('/direction', function () {
    dd(request()->input('direction'));
});



Route::get('/loop2', function () {
    $x = 5;
    $sum = 0;
    $counter = 0;
    while ($counter < $x) {
        $sum = $sum + $counter * 2;
        $counter = $counter + 1;
    }
    echo $sum;
});

Route::get('/exercise3', function () {
    $x = 1000;
    $sum = 0;
    $counter = 0;
    while ($counter < $x) {
        // If $counter is even?
        if ($counter % 2 == 0) {
            echo $counter . ": EVEN<br>";
            $sum = $sum + $counter;
        } else { // $counter is odd
            echo $counter . ": ODD<br>";
        }

        $counter = $counter + 1;
    }
    echo "The sum is: " . $sum;
});

Route::get('/while-sample', function () {
    $number = 10;
    while ($number >= 0) {
        echo $number . "<br>";
        $number = $number - 1;
    }
    echo "Have a nice day!";
});

Route::get('/for-sample', function () {
    for ($number = 10; $number > 0; $number = $number - 1) {
        echo $number . "<br>";
    }
    echo "Have a nice day!";
});

Route::get('/loop', function () {
    $x = 5;
    $sum = 0;
    $counter = 0;
    while ($counter < $x) {
        $sum = $sum + $counter;
        $counter = $counter + 1;
    }
    echo $sum;
});

Route::get('/for-loop-exercise/', function () {
    $sum = 0;

    for ($number = 0; $number < 10; $number = $number + 1) {
        $sum = $sum + $number;
        echo $number . " Hello<br>";
    }

    echo "Have a nice day!";
    echo $sum;
});

Route::get('/for-loop-in-blade-file', function () {
    return view('loop');
});

Route::get('/array-sample', function () {
    $names = [
        "Alyanna",
        "Breanna",
        "Erin",
        "Kian",
        "Natalie"
    ];

    // $numberOfNames = count($names);
    // $index = 0;
    // echo "This is using while loop<br>";
    // while ($index < $numberOfNames) {
    //     echo $names[$index] . "<br>";
    //     $index = $index + 1;
    // }
    // $index2 = 0;
    // echo "<br>This is using for loop<br>";
    // for ($index2 = 0; $index2 < $numberOfNames; $index2 += 1) {
    //     echo $names[$index2] . "<br>";
    // }
    $fruit = "Orange";
    return view('array_sample', [
        'names' => $names,
        'breanna' => 100,
        'alyanna' => $fruit,
    ]);
});

Route::get('array-exercises', function () {
    return view('array_exercises');
});



Route::get('food_two', function () {
    $names = ["Cloud", "Barret", "Tifa", "Aerith"];
    $index = 0;
    while ($index < 4) {
        $currentName = $names[$index];
        echo $currentName . " loves eggs. They are " . $currentName . "'s favorite things in the world to eat.<br>";
        $index = $index + 1;
    }
});

Route::get('food_three', function () {
    $names = ["Cloud", "Barret", "Tifa", "Aerith"];
    $foods = ["eggs", "carrots", "potatoes", "cucumbers"];
    $quantities = [3, 2, 4, 5];
    $index = 0;

    while ($index < 4) {
        $currentName = $names[$index];
        $currentFood = $foods[$index];
        echo $currentName . " loves " . $currentFood . ". They are " . $currentName . "'s favorite things in the universe to eat.<br>";
        echo "$currentName ate $quantities[$index] $foods[$index] today.<br>";
        echo "<br>";
        $index = $index + 1;
    }
});

Route::get('food_four', function () {
    $names = ["Eren", "Mikasa", "Armin", "Levi"];
    $foods = ["eggs", "carrots", "potatoes", "cucumbers"];
    $quantities = [3, 2, 4, 5];
    $index = 0;

    while ($index < 4) {
        $currentName = $names[$index];
        $currentFood = $foods[$index];
        echo $currentName . " loves " . $currentFood . ". They are " . $currentName . "'s favorite things in the universe to eat.<br>";
        echo "$currentName ate $quantities[$index] $foods[$index] today.<br>";
        echo "<br>";
        $index = $index + 1;
    }
});

Route::get('food_one', function () {
    $names = ["Howell", "Cloud", "Aerith"];
    $foods = ["apples", "carrots", "cucumbers"];
    for ($counter = 0; $counter < count($names); $counter += 1) {
        echo getFoodStatement($names[$counter], $foods[$counter]);
    }
});

function getFoodStatement($name, $food): string
{
    return "$name loves $food<br>";
}

Route::get('food_x', function () {
    // echo getCaloriesStatement()
});

// Create a function that accepts name, food, quantity, calories per unit, then returns the statement
// My name is Howell. I love eggs. I ate 10 eggs today. One egg is 78 calories. I ate a total of 780 calories.
function getCaloriesStatement($name, $food, $quantity, $caloriesPerUnit): string
{
    return "";
    //  My name is Howell. I love eggs. I ate 10 eggs today. One egg is 78 calories. I ate a total of 780 calories.
}

// Create a function isOver9000 that accepts an integer value, and returns true if it exceeds 9000, and false otherwise.

Route::get('exercise_1', function () {
    $numbers = [1, 4, 7, 9, 10];
    $sum = 0;
    for ($index = 0; $index < count($numbers); $index += 1) {
        $sum = $sum + $numbers[$index];
    }
    echo $sum;
});

Route::get('exercise_2_for', function () {
    $numbers = [1, 4, 7, 9, 10];
    for ($index = count($numbers) - 1; $index >= 0; $index -= 1) {
        echo $numbers[$index] . " ";
    }
});

Route::get('exercise_2_while', function () {
    $numbers = [1, 4, 7, 9, 10];
    $index = count($numbers) - 1; // 4
    while ($index >= 0) {
        echo $numbers[$index] . " ";
        $index -= 1;
    }
});

Route::get('exercise_3', function () {
    return view('exercise_3', [
        'numbers' => [1, 4, 7, 9, 10]
    ]);
});

Route::get('2021-09-16', function () {
    // return view('2021-09-16');
});

Route::get('exercise_4', function () {
    $names = ["Alyanna", "Erin", "Breanna", "Kian", "Natalie"];
    $search = "Erin";
    $index = 0;
    $message = "Not Found";
    while ($index < count($names) && $message == 'Not Found') {
        $name = $names[$index];

        echo "We are comparing $name with $search...<br>";
        if ($name == $search) {
            // name is found
            $message = "Found";
        }
        $index += 1;
    }
    // We are outside the loop
    echo $message;
});

Route::get('boolean_operators', function () {
    $value = (false || false || true || false) && (true && false);
    // !=
    // &&
    // || 
    // !
    // 15 % 7 => 1
    // 15 div 7 => 2
    // true && false ==> false
    if ($value == true) {
        echo "True";
    } else {
        echo "False";
    };
});




Route::get('oop', function () {
    // An object is a blueprint
    // The object can have attributes and functions
    // For example, we have an object Student
    // Attributes describe the object
    // first_name, last_name, gender, date_of_birth
    // Functions/Methods are actions that the object can do
    // enroll, eat, drink, dance
    // instantiate an object
    $estudyante1 = new App\Helpers\Student;
    $estudyante1->firstName = "Kian";
    $estudyante1->lastName = "Chua";
    $estudyante1->gender = "M";

    $estudyante2 = new App\Helpers\Student;
    $estudyante2->firstName = "Natalie";
    $estudyante2->lastName = "Chan";
    $estudyante2->gender = "F";

    $estudyante1->sayHello();
    echo "<br>";
    $estudyante2->sayHello();
    echo "<br>";

    echo "The full name of estudyante1 is: ";
    echo $estudyante1->getFullName();

    $computerClass = new App\Helpers\Classroom;
    $computerClass->addStudent($estudyante1);
    $computerClass->addStudent($estudyante2);

    echo "<br>";
    echo "Class list: ";
    echo "<br>";
    $computerClass->listStudents();
});

Route::get('collection_example', function () {
    $sampleCollection = new App\Helpers\IBCollection;
    $sampleCollection->addItem("Kian");
    if ($sampleCollection->isEmpty()) {
        echo "YES";
    } else {
        echo "NO";
    }
    $sampleCollection->addItem("Alyanna");
    $sampleCollection->addItem("Natalie");
    echo "1: " . $sampleCollection->getNext();
    echo "<br>";
    echo "2: " . $sampleCollection->getNext();
    echo "<br>";
    echo "3: " . $sampleCollection->getNext();
});

Route::get('collection_example', function () {
    $sampleCollection = new App\Helpers\IBCollection;
    $sampleCollection->addItem("Kian");
    if ($sampleCollection->isEmpty()) {
        echo "YES";
    } else {
        echo "NO";
    }
    $sampleCollection->addItem("Alyanna");
    $sampleCollection->addItem("Natalie");
    echo "1: " . $sampleCollection->getNext();
    echo "<br>";
    echo "2: " . $sampleCollection->getNext();
    echo "<br>";
    echo "3: " . $sampleCollection->getNext();
});

Route::get('transport_authority', function () {
    $passengers = new App\Helpers\IBCollection;
    $passengers->addItem(212);
    $passengers->addItem(454);
    $passengers->addItem(342);
    $passengers->addItem(678);
    $passengers->addItem(213);
    $passengers->addItem(199);
    $passengers->addItem(300);
    $passengers->addItem(120);
    $passengers->addItem(360);

    // b) populate pArray with the items from the collection
    $pArray = [];
    while ($passengers->hasNext()) {
        $pArray[] = $passengers->getNext();
    }

    // c) Find the day with the highest average number of passengers

    // c.1) For a given day of the week, how many passengers?
    $dayOfWeekPassengers = [0, 0, 0, 0, 0, 0, 0];

    // c.2) For a given day of the week, how many times?
    $dayOfWeekCount = [0, 0, 0, 0, 0, 0, 0];

    // c.3) For a given of the week, what is average passenger count?
    $dayOfWeekAverages = [0, 0, 0, 0, 0, 0, 0];

    for ($dayOfMonthIndex = 0; $dayOfMonthIndex < count($pArray); $dayOfMonthIndex += 1) {
        $dayOfWeekIndex = $dayOfMonthIndex % 7;

        $passengersForTheDay = $pArray[$dayOfMonthIndex];

        $dayOfWeekPassengers[$dayOfWeekIndex] = $dayOfWeekPassengers[$dayOfWeekIndex] + $passengersForTheDay;
        $dayOfWeekCount[$dayOfWeekIndex] = $dayOfWeekCount[$dayOfWeekIndex] + 1;
    }

    for ($dayOfWeekIndex = 0; $dayOfWeekIndex < 7; $dayOfWeekIndex += 1) {
        $dayOfWeekAverages[$dayOfWeekIndex] = $dayOfWeekPassengers[$dayOfWeekIndex] / $dayOfWeekCount[$dayOfWeekIndex];
    }

    $highestNumber = 0; // Highest average passengers
    $highestDay = 0;    // Day with the highest average of passengers
    for ($dayOfWeekIndex = 0; $dayOfWeekIndex < 7; $dayOfWeekIndex += 1) {
        $average = $dayOfWeekAverages[$dayOfWeekIndex];
        if ($average > $highestNumber) {
            $highestNumber = $average;
            $highestDay = $dayOfWeekIndex;
        }
    }

    echo convert($highestDay);
});

function convert($day)
{
    return [
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday',
        'Sunday'
    ][$day];
}
