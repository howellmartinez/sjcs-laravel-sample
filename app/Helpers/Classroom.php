<?php

namespace App\Helpers;

class Classroom
{
    public $students = [];

    public function addStudent(Student $student)
    {
        $this->students[] = $student;
    }

    public function listStudents()
    {
        for ($i = 0; $i < count($this->students); $i += 1) {
            $student = $this->students[$i];
            echo $student->getFullName() . "<br>";
        }
    }
}
