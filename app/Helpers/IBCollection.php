<?php

namespace App\Helpers;

class IBCollection
{
    private $items = [];
    private $nextIndex = 0;

    public function __construct()
    {
        $this->nextIndex = 0;
    }

    public function addItem($item)
    {
        $this->items[] = $item;
    }

    public function getNext()
    {
        $temp = $this->items[$this->nextIndex];
        $this->nextIndex += 1;
        return $temp;
    }

    public function resetNext()
    {
        $this->nextIndex = 0;
    }

    public function hasNext()
    {
        $lastIndex = count($this->items) - 1;
        return $this->nextIndex <= $lastIndex;
    }

    public function isEmpty()
    {
        return count($this->items) == 0;
    }
}
