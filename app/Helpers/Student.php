<?php

namespace App\Helpers;

class Student
{
    public $firstName;
    public $lastName;
    public $gender;

    public function sayHello()
    {
        echo "Hello! My name is $this->firstName";
    }

    public function getFullName()
    {
        return $this->firstName . ' ' . $this->lastName;
    }
}
