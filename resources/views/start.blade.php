<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Start Page</title>
</head>

<body>
    <h1>Start Page</h1>
    <form method="POST" action="/weapon">
        @csrf

        <label>Choose your weapon:</label>
        <input type="radio" name="weapon" value="Sword">
        <img width="200" src="https://upload.wikimedia.org/wikipedia/commons/3/3a/Trp-Sword-14226124129-v06.png">

        <input type="radio" name="weapon" value="Axe">
        <img width="200" src="https://cf.shopee.ph/file/8d63934b197be729c314adcd9ac54e25">

        <button type="submit">Go!</button>
    </form>

</body>

</html>
