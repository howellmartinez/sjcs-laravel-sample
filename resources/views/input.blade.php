<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>The Input Page</title>
</head>

<body>
    <h1>Input your name</h1>
    <form method="POST" action="/accept">
        @csrf
        <label>Dog Name:</label>
        <input type="text" name="dog_name" placeholder="Enter your dog's name">

        <label>Dog Age:</label>
        <input type="text" name="dog_age" placeholder="Enter your dog's age">

        <label>Dog Gender:</label>
        <input type="text" name="dog_gender" placeholder="Enter your dog's gender">

        <button type="submit">Go!</button>
    </form>

</body>

</html>
