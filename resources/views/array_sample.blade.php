<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Array Sample</title>
</head>

<body>
    <h2>{{ $breanna }}</h2>
    <h2>{{ $alyanna }}</h2>
    You are in array sample.<br>
    @for ($index = 0; $index < 5; $index += 1)
        {{ $names[$index] }}
    @endfor
</body>

</html>
