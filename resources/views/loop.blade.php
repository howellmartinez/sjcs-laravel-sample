<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Loop</title>
</head>

<body>
    Let us demonstrate a loop here
    <img src="https://images.unsplash.com/photo-1574158622682-e40e69881006?ixid=MnwxMjA3fDB8MHxzZWFyY2h8N3x8Y2F0fGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60"
        width="200">
    @for ($number = 1; $number < 40; $number = $number + 2)
        <br>
        <span style="font-size: {{ $number }}px">
            Hello, we are using fort size {{ $number }}
        </span>
        <a href="http://www.google.com/page={{ $number }}">
            Go to Google Page number {{ $number }}
        </a>
    @endfor
</body>

</html>
