<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Exercise 3</title>
</head>

<body>
    <h1>
        Exercise 3
    </h1>

    <table border="1">
        <tr>
            <td>Index</td>
            @for ($index = 0; $index < count($numbers); $index += 1)
                <td>{{ $index }}</td>
            @endfor
        </tr>
        <tr>
            <td>Value</td>
            @for ($index = 0; $index < count($numbers); $index += 1)
                <td>{{ $numbers[$index] }}</td>
            @endfor
        </tr>
    </table>
</body>

</html>
