<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Results Page</title>
</head>

<body>
    <h1>Results Page</h1>
    <h2>
        {{ $dogName }} is {{ $dogAge }} years old. In human years, {{ $pronoun }} would be
        {{ $dogHumanAge }} years
        old.
    </h2>
</body>

</html>
