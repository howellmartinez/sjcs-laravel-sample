<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Path</title>
</head>

<body>
    <h1>Path</h1>

    You chose the weapon {{ $weapon }}

    <form method="POST" action="/direction">
        @csrf

        <label>Choose your direction:</label>
        <input type="radio" name="direction" value="left">Left

        <input type="radio" name="direction" value="right">Right

        <button type="submit">Go!</button>
    </form>

</body>

</html>
