<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Array Exercises</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css"
        integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
</head>

<body class="container">
    <h1>Array Exercises</h1>
    <ol>
        <li>
            <p>
                Problem: Given an array of numbers <code>$numbers = [1, 4, 7, 9, 10];</code> Loop through the array and
                print out the sum of the numbers.
            </p>
            <p>
                Output: 21
            </p>
        </li>
        <li>
            <p>
                Problem: Given an array <code>$numbers = [1, 4, 7, 9, 10];</code> Print the numbers in reverse order.
            </p>
            <p>
                Output: 10 9 7 4 1
            </p>
        </li>
        <li>
            <p>
                Problem: Given an array <code>$numbers = [1, 4, 7, 9, 10];</code> Display the values in a table (blade
                file), with their corresponding index.
            </p>
            <p>
                Output:<br>
            <table class="table table-bordered">
                <tr>
                    <td>Index</td>
                    <td>0</td>
                    <td>1</td>
                    <td>2</td>
                    <td>3</td>
                    <td>4</td>
                </tr>
                <tr>
                    <td>Value</td>
                    <td>1</td>
                    <td>4</td>
                    <td>7</td>
                    <td>9</td>
                    <td>10</td>
                </tr>
            </table>
            </p>
        </li>
        <li>
            <p>
                Problem: Given an array <code>$names = ["Alyanna", "Breanna", "Erin", "Kian", "Natalie"];</code>
                and a variable <code>$search</code>, print "Found" if the value of <code>$search</code> is found in the
                list. Otherwise print "Not Found"
            </p>
            <p>
                Input: <code>$search = "Aki"</code>
                <br>
                Output: Not Found
            </p>
            <p>
                Input: <code>$search = "Erin"</code>
                <br>
                Output: Found
            </p>
        </li>
        <li>
            <p>
                Problem: In a page, show a text input to receive a student name, and a button to submit that input. When
                the form is submitted, check to see if the name exists in the <code>$students</code> array. Redirect to
                a page displaying the student name and their corresponding hobby (which has the same index) if found.
                <code>$students = ["Alyanna", "Breanna", "Erin", "Kian", "Natalie"];</code>
                <code>$hobbies = ["Painting", "Dancing", "Reading", "Basketball", "Anime"];</code>
                Otherwise, display not found.
            </p>
            <p>
                <label>Name:</label>
                <input type="text" value="Alyanna"><button type="button">Search</button>
                <br><br>
                Output: Redirect to new page which displays<br>
            <h2>Alyanna likes Painting</h2>
            </p>
            <p>
                <label>Name:</label>
                <input type="text" value="Kuma"><button type="button">Search</button>
                <br><br>
                Output: Redirect to new page which displays<br>
            <h2>Kuma not found</h2>
            </p>
        </li>


    </ol>
</body>

</html>
