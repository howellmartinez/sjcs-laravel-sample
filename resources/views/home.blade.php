<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>The Home Page</title>
</head>

<body>
    <h1>Input your details</h1>
    <form method="POST" action="/display">
        @csrf
        <label>Age:</label>
        <input type="number" name="age" placeholder="Enter your age">

        <label>PWD:</label>
        <input type="checkbox" name="pwd">

        <label>Gender:</label>
        <input type="radio" name="gender" value="M">M
        <input type="radio" name="gender" value="F">F

        <label>Birthday:</label>
        <input type="date" name="birthday">

        <label>City:</label>
        <select name="city">
            <option value="Manila">Manila</option>
            <option value="Quezon City">
                What you want your user to see
            </option>
            <option value="Makati">Makati</option>
            <option value="Pasay">Pasay</option>
            <option value="Taguig">Taguig</option>
        </select>

        <button type="submit">Go!</button>
    </form>
    <a href="/string-comparison">
        Go to Google.com
    </a>

</body>

</html>
