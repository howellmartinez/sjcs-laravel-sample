<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Display Page</title>
</head>

<body>
    <h1>Display Page</h1>
    <h2>
        Age Message: {{ $age_message }}
        <br>
        STATUS: {{ $status_message }}
        <br>
        GENDER: {{ $gender }}
        <br>
        CITY: {{ $city }}
        <br>
        BIRTHDAY: {{ $birthday }}
    </h2>
    @if ($gender == 'M')
        <img src="/dog.jpg">
    @elseif ($gender == 'F')
        <img src="/turtle.jpeg">
    @endif
    <br>

    @if ($city == 'Manila')
        <h2>Isko</h2>
    @else
        <h2>Someone else</h2>
    @endif

    <a href="/home">Go back to home</a>
</body>

</html>
